#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <cstdlib>

template<class Iterator>
void myBubbleSort(Iterator begin, Iterator end){
    for (Iterator i = begin; i != end; ++i)
        for (Iterator j = begin; j < i; ++j)
            if (*i < *j)
                std::iter_swap(i, j);
}

template<class T>
void cout_vector(std::ostream& out, std::vector<T> vec){
    out << "[";
    for(int i = 0; i < vec.size(); i++){
        out << vec[i];
        if (i != vec.size() - 1){
            out << ", ";
        }
    }
    out << "]";
    out << std::endl;

}



int main() {
    //deleted stupid changes
    std::ofstream file_out("/home/bogodan/CLionProjects/OOP_spring_module/text.txt");
    if (!file_out)
    {
        // то выводим следующее сообщение об ошибке и выполняем функцию exit()
        std::cerr << "Uh oh, SomeText.txt could not be opened for writing!" << std::endl;
        exit(1);
    }
    srand(time(0));
    int amount ;
    std::cin >> amount;
    std::vector<int> container;
    for (int i = 0; i < amount; i++){
        container.push_back(rand()%90 + 10);
    }
    cout_vector(std::cout, container);
    cout_vector(file_out, container);

    myBubbleSort(container.begin(), container.end());

    cout_vector(std::cout, container);
    cout_vector(file_out, container);
    return 0;
}

